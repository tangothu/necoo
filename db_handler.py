import mysql.connector
import utils as ut

#obsahuje tridu pro manipulaci s databazi

class DBHandler: #konstruktor tridy DBHandler
    def __init__(self, config):
        self.config = ut.load_config_json(config)
        self.connection = None
        self.open_connection()
    # prijima argument config, ktery je cestou k souboru s konfiguraci
    # nacte konfiguraci z json souboru pomoci pomocne funkce load_config_json z utils.py a ulozi ji do promenne self.config
    # vola metodu open_connection k navazani spojeni s databazi

    def open_connection(self):
        self.connection = mysql.connector.connect(**self.config)
    # navazani spojeni s databazi pomoci modulu mysql.connector a metody connect, ktera prijima slovnik s parametry pro pripojeni
    # pripojeni je ulozeno do promenne self.connection

    def close_connection(self):
        if self.connection:
            self.connection.close()
    # zavreni aktivniho pripojeni s databazi, pokud je spojeni otevreno

    def execute_query(self, sql, params=None):
        self.open_connection()
        cursor = self.connection.cursor()
        cursor.execute(sql, params)
        data = cursor.fetchall() if sql.strip().upper().startswith('SELECT') else None
        self.connection.commit()
        cursor.close()
        return data
    # univerzalni metoda zopodvedna za vykonavani jakehokoli SQL prikazu zadaneho argumentem sql
    # nepovinny argument params muze byt pouzit pro bezpecne predani parametru do sql dotazu
    # funkce vraci data pouze v pripade, ze se vykonava SELECT prikaz
    # v ostatnich pripadech (INSERT, DELETE, UPDATE) vraci None
    # po vykonani dotazu funkce ulozi zmeny v databazi volanim self.connection.commit() a uzavre cursor

    def insert_data(self, sql, params):
        self.execute_query(sql, params)
    # 'insert_data' slouzi k vlozeni dat do databaze
    # argument 'sql' by mel obsahovat prikaz pro vlozeni dat a 'params' by mel byt seznam s daty, ktera chceme vlozit
    # interne se tato metoda vola 'execute_query' pro vykonani operace

    def select_data(self, sql):
        return self.execute_query(sql)
    # 'select_data' slouzi k vyberu dat z databaze
    # argument 'sql' by mel obsahovat prikaz pro vyber dat

    def delete_data(self, sql, params):
        self.execute_query(sql, params)
    # 'delete_data' slouzi k mazani dat z databaze
    # argument 'sql' by mel obsahovat prikaz pro smazani dat a 'params' by mel byt seznam s daty, ktera chceme smazat

    
