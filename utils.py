import json
from pathlib import Path

# pomocne funkce pro nacteni konfigurace a ulozeni vysledku do souboru

def load_config_json(path):
    with open(path, 'r') as f:
        return json.load(f)
    
def save_to_file(data):
    file_name = Path(__file__).parent.joinpath('data/output.txt')
    with open(file_name, 'w') as f:
        f.write(data)