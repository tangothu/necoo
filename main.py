import db_handler as dbh
from pathlib import Path
from fastapi import FastAPI, Response
import uvicorn
from io import StringIO
import csv


app = FastAPI()

@app.get("/user") #endpoint se musi jmenovat jako nazev tabulky, ze ktery to bude tahat data
def get_user():
    return db.select_data("SELECT * FROM user")

@app.get("/user/csv")
def get_user_csv():
        # Získání dat z databáze
    data = db.select_data("SELECT * FROM user")
    # Vytvoření CSV souboru v paměti
    output = StringIO() #musi byt!!! jinak to nefunguje
    writer = csv.writer(output)
    # Přidání hlaviček - předpokládáme, že znáte názvy sloupců, jinak je získejte dynamicky
    writer.writerow(["id", "name", "surname", "insurance"])  # Příklad sloupců
    # Přidání dat
    writer.writerows(data)
    output.seek(0)  # Reset kurzoru na začátek souboru
    # Nastavení hlaviček pro stažení souboru
    headers = {
        'Content-Disposition': 'attachment; filename="users.csv"'
    }
    return Response(content=output.read(), media_type="text/csv", headers=headers)

@app.get("/user/{id}")
def get_user_by_id(id: int):
    return db.select_data(f"SELECT * FROM user where id = {id}")




if __name__ == "__main__":
    db = dbh.DBHandler(Path(__file__).parent.joinpath('config/db_config.json'))
    uvicorn.run(app, host="0.0.0.0", port=8000)
    